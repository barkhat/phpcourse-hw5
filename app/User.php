<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = ['username', 'firstname', 'lastname', 'sex', 'email', 'password', 'phoneNumber', 'avatar'];
}
