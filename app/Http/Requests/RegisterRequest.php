<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' =>  'required|unique:users',
            'firstname' => 'nullable|alpha|max:30',
            'lastname' => 'nullable|alpha|max:30',
            'sex' => [
                'nullable',
                Rule::in(['мужской', 'женский'])
            ],
            'email' => 'email|unique:users',
            'password' => [
                'required',
                'confirmed',
                'min:8',
                'max:50',
                'regex:/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/'
            ],
            'phoneNumber' => 'nullable|digits:10',
        ];
    }

    public function messages()
    {
        return [
            'username.required' => 'Username обязателен',
            'username.unique' => 'Пользователь с таким username уже существует',
            'firstname.alpha' => 'Имя должно содержать только буквы',
            'firstname.max' => 'Максимальная длина имени: 30',
            'lastname.alpha' => 'Имя должно содержать только буквы',
            'lastname.max' => 'Максимальная длина имени: 30',
            'sex.in' => 'Пол может быть только "мужской" или "женский"',
            'email.email' => 'Email неправильного формата',
            'password.required' => 'Пароль обязателен',
            'password.min' => 'Минимальная длина пароля: 8',
            'password.max' => 'Максимальная длина пароля: 50',
            'password.regex' => 'Пароль должен содержать строчные и прописные латинские буквы, цифры',
            'phoneNumber.digits' => 'Номер телефона должен состоять только из цифр'
        ];
    }
}
