<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login' => [
                'required',
                'regex:/^[a-zA-Z0-9_.+-]+(@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)?$/'
            ],
            'password' => [
                'required',
                'min:8',
                'max:50',
                'regex:/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/'
            ]
        ];
    }

    public function messages()
    {
        return [
            'login.required' => 'Логин обязателен',
            'login.regex' => 'Логин неправильного формата',
            'password.required' => 'Пароль обязателен',
            'password.min' => 'Минимальная длина пароля: 8',
            'password.max' => 'Максимальная длина пароля: 50',
            'password.regex' => 'Пароль должен содержать строчные и прописные латинские буквы, цифры'
        ];
    }
}
