<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 08.07.2017
 * Time: 23:42
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRequest;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    public function authenticate(AuthRequest $request)
    {
        $user = User::where('username', '=', $request->login)
                    ->orWhere('email', '=', $request->login)
                    ->first();

        if (isset($user) and Hash::check($request->password, $user->password)) {
            $request->session()->put('user', $user);
            return redirect()->route('dashboard', ['username' => $user->username]);
        } else {
            return Redirect::back()->withErrors(['Username/Email or password is invalid']);
        }
    }
}