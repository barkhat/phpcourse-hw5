<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\User;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function addUser(RegisterRequest $request)
    {
        $newUser = new User;

        $newUser->username = $request->username;
        $newUser->firstname = $request->firstname;
        $newUser->lastname = $request->lastname;
        $newUser->sex = $request->sex;
        $newUser->email = $request->email;
        $newUser->password = Hash::make($request->password);
        $newUser->phoneNumber = $request->phoneNumber;

        $newUser->save();

        $request->session()->put('user', $newUser);
        return view('dashboard', ['username' => $newUser->username, 'request' => $request]);
    }
}
