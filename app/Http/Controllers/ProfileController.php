<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateProfileRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function show($username, Request $request)
    {
        if ($request->session()->get('user')->username === $username or $request->session()->get('user')->isAdmin) {
            $user = User::where('username', '=', $username)->first();
            if ($user) {
                return view('profile', ['user' => $user, 'request' => $request]);
            } else {
                return 'User ' . $username . ' not found';
            }
        } else {
            return 'You are prohibited';
        }
    }

    public function edit($username, UpdateProfileRequest $request)
    {
        $user = User::where('username', '=', $username)->first();
        if ($request->isMethod('get')) {
            return view('editprofile', ['user' => $user, 'request' => $request]);
        }
        if ($request->isMethod('post')) {
            $user->firstname = $request->firstname;
            $user->lastname = $request->lastname;
            $user->sex = $request->sex;
            $user->phoneNumber = $request->phoneNumber;
            $user->email = $request->email;
            if ($request->password) {
                $user->password = Hash::make($request->password);
            }
            if ($request->file('avatar')) {
                $user->avatar = $request->file('avatar')->store('public/avatars');
            }
            $user->save();

            return redirect("user/$user->username/profile");
        }
    }
}
