@extends('app')

@section('title', 'Редактирование профиля')

@section('custom_style')

    .form-profile {
        padding: 15px;
        margin: 0 auto;
    }

@endsection

@section('content')
    <form class="form-horizontal form-profile" enctype="multipart/form-data" method="post">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-3">
                <img class="img-rounded" id="imgAvatar" src="{{ Storage::url($user->avatar) }}" height="150" width="150">
                <div class="form-group">
                    <label for="inputAvatar">Аватарка</label>
                    <input type="file" name="avatar" id="inputAvatar">
                </div>

            </div>
            <div class="col-md-6">
            <div class="form-group">
                <label>Логин</label>
                <input class="form-control" type="text" placeholder="{{ $user->username }}" readonly>
            </div>
            <div class="form-group">
                <label for="inputFirstname">Имя</label>
                <input class="form-control" id="inputFirstname" name="firstname" type="text" name="firstname"
                       value="{{ $user->firstname }}" placeholder="Имя">
            </div>
            <div class="form-group">
                <label for="inputLastname">Фамилия</label>
                <input class="form-control" id="inputLastname" name="lastname" type="text" value="{{ $user->lastname }}"
                       placeholder="Фамилия">
            </div>
            <div class="form-group">
            <label for="inputSex">Пол</label>
            <select class="form-control" id="inputSex" name="sex">
                @if ($user->sex === 'мужской')
                    <option value="мужской" selected>Мужской</option>
                    <option value="женский">Женский</option>
                @elseif ($user->sex == 'женский')
                    <option value="мужской">Мужской</option>
                    <option value="женский" selected>Женский</option>
                @else
                    <option disabled selected>Выберите пол</option>
                    <option value="мужской">Мужской</option>
                    <option value="женский">Женский</option>
                @endif
            </select>
            </div>
            <div class="form-group">
                <label for="inputEmail">Email</label>
                <input class="form-control" id="inputEmail" name="email" type="email" value="{{ $user->email }}" required>
            </div>
            <div class="form-group">
                <label for="inputPassword">Пароль (строчные и прописные латинские буквы, цифры)</label>
                <input class="form-control" id="inputPassword" name="password" type="password">
            </div>
                <div class="form-group">
                    <label for="inputConfirmPassword">Подтверждение пароля</label>
                    <input class="form-control" id="inputConfirmPassword" name="password_confirmation" type="password">
                </div>
            <div class="form-group">
                <label for="inputPhoneNumber">Номер телефона</label>
                <input class="form-control" id="inputPhoneNumber" name="phoneNumber" type="text" value="{{ $user->phoneNumber }}" placeholder="Номер телефона">
            </div>
            <input type="submit" class="btn btn-primary" value="Обновить профиль">
            </div>
        </div>
    </form>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#imgAvatar').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#inputAvatar").change(function () {
            readURL(this);
        });
    </script>
@endsection