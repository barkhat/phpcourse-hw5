@extends('app')

@section('title', 'Личный кабинет')

@section('content')
  <h1>Привет {{ $username }}</h1>
  @if (Session::get('user')->isAdmin)
    <h3>Список пользователей</h3>
    <ul class="list-group">
      @foreach(\App\User::where('username', '!=', 'admin')->get() as $user)
        <li class="list-group-item"><a href="/user/{{ $user->username }}/profile">{{ $user->username }}</a></li>
      @endforeach
    </ul>
  @endif
@endsection
