@extends('app')

@section('title', 'Регистрация')

@section('content')
    <form class="form-signup" method="POST">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-5 col-md-offset-3">
                <h2>Регистрация</h2>
                <div class="form-group">
                    <label for="inputUsername">Логин</label>
                    <input class="form-control" id="inputUsername" name="username" type="text" placeholder="Username"
                           value="{{ old('username') }}" required autofocus>
                </div>
                <div class="form-group">
                    <label for="inputFirstname">Имя</label>
                    <input class="form-control" id="inputFirstname" name="firstname" type="text" name="firstname"
                           value="{{ old('firstname') }}" placeholder="Имя">
                </div>
                <div class="form-group">
                    <label for="inputLastname">Фамилия</label>
                    <input class="form-control" id="inputLastname" name="lastname" type="text"
                           value="{{ old('lastname') }}"
                           placeholder="Фамилия">
                </div>
                <div class="form-group">
                    <label for="inputSex">Пол</label>
                    <select class="form-control" id="inputSex" name="sex">
                        <option disabled selected>Выберите пол</option>
                        <option value="мужской">Мужской</option>
                        <option value="женский">Женский</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="inputEmail">Email</label>
                    <input class="form-control" id="inputEmail" name="email" type="email" value="{{ old('email') }}"
                           required>
                </div>
                <div class="form-group">
                    <label for="inputPassword">Пароль (строчные и прописные латинские буквы, цифры)</label>
                    <input class="form-control" id="inputPassword" name="password" type="password" required>
                </div>
                <div class="form-group">
                    <label for="inputConfirmPassword">Подтверждение пароля</label>
                    <input class="form-control" id="inputConfirmPassword" name="password_confirmation" type="password"
                           required>
                </div>
                <div class="form-group">
                    <label for="inputPhoneNumber">Номер телефона</label>
                    <input class="form-control" id="inputPhoneNumber" name="phoneNumber" type="text"
                           value="{{ old('phoneNumber') }}" placeholder="Номер телефона">
                </div>
                <button class="btn btn-primary" type="submit">Зарегистрироваться</button>
            </div>
        </div>
    </form>
@endsection