@extends('app')

@section('title', 'Войти')

@section('custom_style')
  .form-signin {
    padding: 15px;
    margin: 0 auto;
@endsection

@section('content')
  <div class="col-md-4 col-md-offset-4">
    <form class="form-signin" method="POST">
      {{ csrf_field() }}
      <h2 class="form-signin">Вход</h2>
      <div class="form-group">
        <label for="inputLogin">Email</label>
        <input class="form-control" id="inputLogin" name="login" type="text" placeholder="Email или логин" required
               autofocus>
      </div>
      <div class="form-group">
        <label for="inputPassword">Пароль</label>
        <input class="form-control" id="inputPassword" name="password" type="password" placeholder="Пароль" required>
      </div>
      <button class="btn btn-primary" type="submit">Войти</button>
    </form>
  </div>
@endsection