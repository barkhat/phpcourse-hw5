@extends('app')

@section('title', 'Профиль')

@section('custom_style')
    .content {
    padding: 15px;
    margin: 0 auto;
    }
@endsection

@section('content')
    <div class="content">
    <div class="row">
        <div class="col-md-3">
            <img class="img-rounded" src="{{ Storage::url($user->avatar) }}" height="150" width="150">
        </div>
        <div class="col-md-6">
            <ul class="list-group">
                <li class="list-group-item"><strong>Логин:</strong> {{ $user->username }}</li>
                @if ($user->firstname)
                    <li class="list-group-item"><strong>Имя:</strong> {{ $user->firstname }}</li>
                @else
                    <li class="list-group-item"><strong>Имя:</strong> не указано</li>
                @endif
                @if ($user->lastname)
                    <li class="list-group-item"><strong>Фамилия:</strong> {{ $user->lastname }}</li>
                @else
                    <li class="list-group-item"><strong>Фамилия:</strong> не указано</li>
                @endif
                @if ($user->sex)
                    <li class="list-group-item"><strong>Пол:</strong> {{ $user->sex }}</li>
                @else
                    <li class="list-group-item"><strong>Пол:</strong> не указано</li>
                @endif
                <li class="list-group-item"><strong>Email:</strong> {{ $user->email }}</li>
                @if ($user->phoneNumber)
                    <li class="list-group-item"><strong>Номер телефона:</strong> {{ $user->phoneNumber }}</li>
                @else
                    <li class="list-group-item"><strong>Номер телефона:</strong> не указано</li>
                @endif
            </ul>
            <a class="btn btn-primary" href="/user/{{ $user->username }}/editprofile">Изменить</a>
        </div>
    </div>
    </div>
@endsection


