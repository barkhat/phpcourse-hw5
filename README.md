# Домашняя работа 5 #

В процессе выполнения использовался Homestead

### Создание и заполнение таблицы пользователей ###

```
#!bash


php artisan migrate
php artisan db:seed --class=UsersTableSeeder
```

### Папка с аватарками ###
Для того, чтобы открыть публичный доступ к аватаркам нужно создать символическую ссылку

```
#!bash

php artisan storage:link
```