<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', function (Request $request) {
    return view('home');
});

Route::get('login', function () {
    return view('login');
});

Route::post('login', 'LoginController@authenticate');

Route::get('register', function () {
    return view('register');
});

Route::post('register', 'RegisterController@addUser');

Route::get('signout', function (Request $request) {
    $request->session()->flush();
    return view('home');
});

Route::get('user/{username}', function ($username, Request $request) {
    if ($request->session()->get('user')->username === $username or $request->session()->get('user')->isAdmin) {
        return view('dashboard', ['username' => $username]);
    } else {
        return 'You are prohibited';
    }
})->name('dashboard');

Route::get('user/{username}/profile', 'ProfileController@show')->name('profile');

Route::match(['get', 'post'], 'user/{username}/editprofile', 'ProfileController@edit');