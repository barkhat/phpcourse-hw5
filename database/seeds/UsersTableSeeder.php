<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'admin',
            'email' => 'admin@example.com',
            'password' => Hash::make('a12345678A'),
            'isAdmin' => true
        ]);

        DB::table('users')->insert([
            'username' => 'Max',
            'email' => 'max@example.com',
            'password' => Hash::make('b12345678B'),
            'firstname' => 'Максим',
            'lastname' => 'Воронцов',
            'sex' => 'мужской',
            'phoneNumber' => '9884445798'
        ]);

        DB::table('users')->insert([
            'username' => 'Kate',
            'email' => 'kate@example.com',
            'password' => Hash::make('c12345678C'),
            'firstname' => 'Катерина',
            'lastname' => 'Сидорова',
            'sex' => 'женский',
            'phoneNumber' => '9369995498'
        ]);
    }
}
